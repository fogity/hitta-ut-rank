from setuptools import setup

setup(
    name='Hittaut',
    version='1.0.0',
    packages=['hittaut'],
    install_requires=[
        'flask',
        'matplotlib',
        'numpy',
        'pandas',
        'pony',
        'psycopg2cffi',
        'requests'
    ],
    entry_points={
        'console_scripts': [
            'hittaut-crawler = hittaut.__init__:crawler_main',
            'hittaut-plotter = hittaut.__init__:plotter_main',
            'hittaut-server = hittaut.__init__:server_main'
        ]
    }
)