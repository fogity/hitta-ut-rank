from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from pony.orm import db_session, select
import time
from .database import Contestant, Score

@db_session
def load_data():
    return pd.DataFrame({
        c.full_name: {
            s.time: s.points
            for s in select(s for s in Score if s.name == c.name)
        }
        for c in select(c for c in Contestant).sort_by(Contestant.full_name)
    })

def create_plot():
    data = load_data()

    time = datetime.now()
    data = data.append(pd.DataFrame({c: {time: np.nan} for c in data.columns}))

    data = data.sort_index()

    data = data.fillna(method='ffill')
    data = data.fillna(0)

    ax = data.plot.line(drawstyle='steps')
    ax.tick_params(colors='w')

    fig = plt.gcf()
    fig.set_size_inches(8, 3)

    plt.savefig('static/plot.svg', bbox_inches='tight', facecolor='#295c88')
    plt.close()

def run_plotter(interval):
    while True:
        try:
            create_plot()
        except Exception as e:
            print(datetime.now(), '| Exception!')
            print(e)
        time.sleep(interval)
