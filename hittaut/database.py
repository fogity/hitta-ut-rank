from datetime import datetime
from pony.orm import Database, Optional, Required

database = Database()

class Contestant(database.Entity):
    name = Required(str)
    full_name = Required(str)
    slack_id = Required(str)

class Score(database.Entity):
    name = Required(str)
    time = Required(datetime)
    points = Required(int)
    rank = Required(int)
    sws_rank = Optional(int)

class Stats(database.Entity):
    time = Required(datetime)
    total_contestants = Required(int)
    public_contestants = Required(int)
    sws_contestants = Required(int)
    total_registrations = Required(int)
    public_registrations = Required(int)
    sws_registrations = Required(int)
    total_registration_average = Required(float)
    public_registration_average = Required(float)
    sws_registration_average = Required(float)
    public_registration_median = Required(float)
    sws_registration_median = Required(float)
    

database.bind(provider='postgres', database='hittaut')
database.generate_mapping(create_tables=True)