from datetime import date
from pony.orm import db_session, select
from .database import Contestant

checkpoints = {
    date(2019, 5, 1): 85,
    date(2019, 7, 1): 100,
    date(2019, 8, 15): 115,
    date(2019, 9, 15): 130
}

def calculate_checkpoint_info():
    today = date.today()
    num_checkpoints = 0
    next_release = None

    for d, num in checkpoints.items():
        if d <= today:
            num_checkpoints = num
            continue
        next_release = d
        break

    return num_checkpoints, next_release

@db_session
def load_contestants():
    return {c.name: c for c in select(c for c in Contestant)}