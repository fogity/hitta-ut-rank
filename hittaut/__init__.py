import sys
from .crawler import run_crawler
from .plotter import run_plotter
from .server import run_server

def crawler_main():
    run_crawler(float(sys.argv[1]))

def plotter_main():
    run_plotter(float(sys.argv[1]))

def server_main():
    run_server()