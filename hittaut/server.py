import base64
from datetime import date
from flask import abort, Flask, render_template, request, send_file
import hashlib
import json
import os
from pony.orm import db_session, desc, select
from .common import calculate_checkpoint_info, load_contestants
from .database import Contestant, Score, Stats

app = Flask(__name__, root_path=os.getcwd())

with open('passphrases.json', 'rb') as f:
    passphrases = json.load(f)

leaderboard_refresh_interval = 150

@db_session
def load_scores(contestants):
    return sorted([
        s
        for name in contestants
        for s in select(s for s in Score if s.name == name and s.sws_rank is not None).sort_by(desc(Score.time))[:1]
    ], key=lambda s: s.sws_rank)

@db_session
def load_stats():
    return select(s for s in Stats).sort_by(desc(Stats.time))[:1][0]

def round_number(number, decimals=0):
    n = round(number, decimals)
    if '.' + '0' * decimals in str(n):
        return int(number)
    return n

def calculate_lag(scores, num_checkpoints):
    if not scores:
        return {}
    lag = {
        curr.name: prev.points - curr.points
        for curr, prev in zip(scores[1:], scores)
    }
    lag[scores[0].name] = num_checkpoints - scores[0].points
    return lag

def calculate_stats(stats):
    return [(
        'Number of contestants',
        stats.sws_contestants,
        stats.public_contestants,
        stats.total_contestants
    ), (
        'Percentage of contestants',
        str(round_number(100 * stats.sws_contestants / stats.total_contestants, 1)) + '%',
        str(round_number(100 * stats.public_contestants / stats.total_contestants, 1)) + '%',
        None
    ), (
        'Number of checkpoint registrations',
        stats.sws_registrations,
        stats.public_registrations,
        stats.total_registrations
    ), (
        'Percentage of registrations',
        str(round_number(100 * stats.sws_registrations / stats.total_registrations, 1)) + '%',
        str(round_number(100 * stats.public_registrations / stats.total_registrations, 1)) + '%',
        None
    ), (
        'Average number of registrations',
        round_number(stats.sws_registration_average, 1),
        round_number(stats.public_registration_average, 1),
        round_number(stats.total_registration_average, 1)
    ), (
        'Median number of registrations',
        round_number(stats.sws_registration_median, 1),
        round_number(stats.public_registration_median, 1),
        None
    )]

@app.route('/')
def get_leaderboard():
    phrase = request.args.get('pass')
    if phrase != passphrases['leaderboard']:
        abort(401)
    
    with open('static/leaderboard.css', 'rb') as f:
        stylesheet_hash = base64.urlsafe_b64encode(hashlib.md5(f.read()).digest()).decode('utf-8')

    try:
        with open('static/plot.svg', 'rb') as f:
            plot_hash = base64.urlsafe_b64encode(hashlib.md5(f.read()).digest()).decode('utf-8')
    except:
        plot_hash = 0

    contestants = load_contestants()    
    scores = load_scores(contestants)
    stats = load_stats()
    full_stats = calculate_stats(stats)
    num_checkpoints, next_release = calculate_checkpoint_info()
    lag = calculate_lag(scores, num_checkpoints)

    args = {
        'round_number': round_number,
        'refresh_interval': leaderboard_refresh_interval,
        'stylesheet_hash': stylesheet_hash,
        'plot_hash': plot_hash,
        'contestants': contestants,
        'scores': scores,
        'lag': lag,
        'stats': full_stats,
        'checkpoints': num_checkpoints,
        'next_release': next_release
    }
    return render_template('leaderboard.html', **args)

@app.route('/add_contestant')
def add_candidate():
    phrase = request.args.get('pass')
    if phrase != passphrases['add_contestant']:
        abort(401)

    name = request.args.get('name')
    full = request.args.get('full')
    slack = request.args.get('slack')

    try:
        with db_session:
            Contestant(name=name, full_name=full, slack_id=slack)
    except:
        abort(400)

    return 'Added contestant ' + full

@app.route('/static/<path:path>')
def get_static(path):
    return send_file(os.path.join('static', path))

def run_server():
    app.run(host='0.0.0.0')