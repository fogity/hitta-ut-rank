from datetime import datetime
import itertools
from pony.orm import db_session, desc, select
import re
import requests
import statistics
import time
from .common import load_contestants
from .database import Score, Stats
from .slack import notify_changes

stat_page_url = 'https://www.orientering.se/provapaaktiviteter/hittaut/goteborgcv/'
contestant_total_regex = re.compile(r'<span>([0-9 ]+)<\/span>\s*<br\/>\s*deltagare')
registration_total_regex = re.compile(r'<span>([0-9 ]+)<\/span>\s*<br\/>\s*registreringar')

def grab_stats():
    page_content = requests.get(stat_page_url).text
    contestant_total_match = contestant_total_regex.search(page_content)
    registration_total_match = registration_total_regex.search(page_content)
    contestant_total = int(contestant_total_match.group(1).replace(' ', ''))
    registration_total = int(registration_total_match.group(1).replace(' ', ''))
    return contestant_total, registration_total

contestant_page_url_template = 'https://www.orientering.se/user/allusers/?skip={skip}&location=9'
contestants_per_page = 400
contestant_regex = re.compile(r'class="rank">([0-9]+)<.+"name">([^<]+)<.+"count">([0-9]+)<')

def grab_scores():
    scores = []
    for n in itertools.count(0, contestants_per_page):
        page_content = requests.get(contestant_page_url_template.format(skip=n)).text
        matches = contestant_regex.findall(page_content)
        if not matches:
            break
        scores.extend((n, int(p), int(r)) for r, n, p in matches)
    return scores

def crawl():
    time = datetime.now()
    print(time, '| Starting crawl...')

    total_contestants, total_registrations = grab_stats()
    scores = grab_scores()
    contestants = load_contestants()

    sws_scores = list(filter(lambda s: s[0] in contestants, scores))
    sws_ranks = {name: rank for rank, (name, _, _) in enumerate(sws_scores, 1)}

    public_contestants = len(scores)
    sws_contestants = len(sws_scores)
    public_registrations = sum(points for (_, points, _) in scores)
    sws_registrations = sum(points for (_, points, _) in sws_scores)
    total_registration_average = total_registrations / total_contestants if total_contestants else 0
    public_registration_average = public_registrations / public_contestants if public_contestants else 0
    sws_registration_average = sws_registrations / sws_contestants if sws_contestants else 0
    public_registration_median = statistics.median(points for (_, points, _) in scores) if scores else 0
    sws_registration_median = statistics.median(points for (_, points, _) in sws_scores) if sws_scores else 0

    new_contestant = []
    score_changes = []
    for (name, points, rank) in scores:
        sws_rank = sws_ranks[name] if name in contestants else None
        with db_session:
            old_score = select(s for s in Score if s.name == name).sort_by(desc(Score.time))[:1]
            if not old_score:
                if name in contestants:
                    new_contestant.append((name, points, sws_rank))
                Score(name=name, time=time, points=points, rank=rank, sws_rank=sws_rank)
            elif points != old_score[0].points or rank != old_score[0].rank or sws_rank != old_score[0].sws_rank:
                if name in contestants:
                    if not old_score[0].sws_rank:
                        new_contestant.append((name, points, sws_rank))
                    else:
                        score_changes.append((name, points - old_score[0].points, sws_rank, old_score[0].sws_rank))
                Score(name=name, time=time, points=points, rank=rank, sws_rank=sws_rank)

    with db_session:
        old_stats = select(s for s in Stats).sort_by(desc(Stats.time))[:1]
        if not old_stats or total_contestants != old_stats[0].total_contestants \
                or total_registrations != old_stats[0].total_registrations \
                or sws_contestants != old_stats[0].sws_contestants:
            Stats(time=time, total_contestants=total_contestants, public_contestants=public_contestants,
                sws_contestants=sws_contestants, total_registrations=total_registrations,
                public_registrations=public_registrations, sws_registrations=sws_registrations,
                total_registration_average=total_registration_average,
                public_registration_average=public_registration_average,
                sws_registration_average=sws_registration_average,
                public_registration_median=public_registration_median,
                sws_registration_median=sws_registration_median)

    notify_changes(contestants, sws_scores, sws_ranks, new_contestant, score_changes)
    print(datetime.now(), '| Crawl completed!')

def run_crawler(interval):
    while True:
        try:
            crawl()
        except Exception as e:
            print(datetime.now(), '| Exception!')
            print(e)
        time.sleep(interval)