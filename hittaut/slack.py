import json
import requests
from .common import calculate_checkpoint_info

with open('webhook.json', 'rb') as f:
    webhook = json.load(f)

def format_cp(count):
    return 'a checkpoint' if count == 1 else f'{count} checkpoints'

def get_passed(contestants, scores, ranks, name, change):
    prev = list(filter(lambda s: s[0] == name, scores))[0][1] - change
    passed = [f'<@{contestants[n].slack_id}>' for (n, p, _) in scores if p >= prev and ranks[n] > ranks[name] and n != name]
    if len(passed) == 0:
        return 'no one'
    if len(passed) == 1:
        return passed[0]
    if len(passed) == 2:
        return ' and '.join(passed)
    return ' and '.join([', '.join(passed[:-1]), passed[-1]])

def format_rank(rank):
    return {
        1: 'the lead',
        2: '2nd place',
        3: '3rd place'
    }.get(rank, f'{rank}th place')

def notify_changes(contestants, scores, ranks, new_contestants, score_changes):
    lines = []
    if new_contestants:
        lines.extend(
            f'<@{contestants[name].slack_id}> has joined the challenge with {format_cp(points)}!'
            for (name, points, _) in new_contestants
        )
        lines.append('The leaderboard now looks like this:')
        lines.extend(
            f'{ranks[name]}. <@{contestants[name].slack_id}> ({points})'
            for (name, points, _) in scores
        )
    elif score_changes:
        lines.extend(
            f'<@{contestants[name].slack_id}> just registered {format_cp(points)}!'
            for (name, points, _, _) in score_changes
            if points
        )
        lines.extend(
            f'<@{contestants[name].slack_id}> has passed {get_passed(contestants, scores, ranks, name, points)} and is now in {format_rank(new_rank)}!'
            for (name, points, new_rank, old_rank) in score_changes
            if new_rank < old_rank
        )
        max_points, _ = calculate_checkpoint_info()
        lines.extend(
            f'<@{contestants[name_1].slack_id}> has found all availible checkpoints! 🎉🎉🎉'
            for (name_1, _, _, _) in score_changes
            for (name_2, points, _) in scores
            if name_1 == name_2 and points == max_points
        )

    if not lines:
        return

    requests.post(webhook, json={
        "text": '\n'.join(lines)
    })
